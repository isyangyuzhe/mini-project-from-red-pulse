import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    context: ''
  },
  mutations: {
    save (state, context) {
      state.context = context
    }
  }
})
